&emsp;

> Stata连享会 &ensp;   [课程主页](https://www.lianxh.cn/news/46917f1076104.html)  || [直播视频](http://lianxh.duanshu.com) || [知乎推文](https://www.zhihu.com/people/arlionn/) 

![](https://images.gitee.com/uploads/images/2020/0317/175426_24bc8678_1522177.png)
> 扫码查看连享会最新专题、公开课视频和 100 多个码云计量仓库链接。


&emsp;


### Stata Plus：连老师的 Stata 外部命令集

#### 项目介绍

- 内容：存放了自 2003 一年以来我下载的所有外部命令。
- 更新时间：`2020/2/8 17:06`
- 命令清单：[- 点击查看 -](https://gitee.com/arlionn/StataPlus/blob/master/lian_plus_tree.txt) 

#### 下载
- **地址1**：百度云盘  <https://pan.baidu.com/s/1Ea1l4nCrB3n7GZywA5PU2A> , 提取码：riom 
- **地址2**：坚果云 <https://www.jianguoyun.com/p/DR6o7KcQtKiFCBi7-tUC> , 速度快


#### 使用方法

下载 **plus.rar** 后，与你的 plus 文件夹合并或直接覆盖你的 plus 文件夹。

- **方法1：** 下载 [「plus.rar」](https://pan.baidu.com/s/1-nvAG8ZDihWcKakViug_QQ) 到本地，解压后，放置于 **D:\stata15\ado** 文件夹下即可。若有自建的 **plus** 文件夹，可以将二者合并，或者直接覆盖 (我的外部命令应该更全面一些)。
- **方法2：** 若想同时保留你自己的 **plus** 和我提供的 **plus** 文件夹，则可以将我的重命名为 **plus2**，然后在 **profile.do** （存放于 `D:\stata15` 目录下）添加如下语句：`adopath + "D:\stata15\ado\plus2"`(绝对路径)，或者 `adopath + "c(sysdir_stata)\ado\plus2"` (相对路径)。重启 Stata 后即可保证 **plus2** 中的命令生效。
- ps，使用过程中可能遇到的问题，都可以在这里找到解答：[[Stata: 外部命令的搜索、安装与使用]](https://zhuanlan.zhihu.com/p/48703028)

### 文件路径相关说明

> plus 文件夹的存放位置

输入 `sysdir` 可以查看你的 plus 文件夹存放于何处。我的文件路径如下：
```stata
. sysdir
   STATA:  D:\stata15\
    BASE:  D:\stata15\ado\base\
    SITE:  D:\stata15\ado\site\
    PLUS:  D:\stata15/ado\plus\
PERSONAL:  D:\stata15/ado\personal\
OLDPLACE:  c:\ado\
```

> Stata 能够识别的 ado 文件存放位置

输入 `adopath` 可以查看 Stata 能够识别的所有 ado 文件的存放位置。如下是我电脑中的设置：

```stata
. adopath
  [1]  (BASE)      "D:\stata15\ado\base/"
  [2]  (SITE)      "D:\stata15\ado\site/"
  [3]              "."
  [4]  (PERSONAL)  "D:\stata15/ado\personal/"
  [5]  (PLUS)      "D:\stata15/ado\plus/"
  [6]  (OLDPLACE)  "c:\ado/"
  [7]              "D:\stata15/\ado\personal\_myado"
```



&emsp; 
> #### 连享会-直播间上线了！随时随地，享受学习！             
> #### <http://lianxh.duanshu.com>      
>    &emsp;      
<img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">
>    &emsp;      
> 长按二维码观看最新直播课     
> 免费公开课：「[直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)」- 连玉君，时长：1小时40分钟。

---
## 课程一览   


> 支持回看，往期精彩课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| Python+Github | 司继春 | [Python和Github入门](https://lianxh.duanshu.com/#/brief/course/132a12b2e5ef45b795bfa897c037a6f4) <br> 两小时, 9.9元
| **文本/爬虫** | 游万海<br>司继春 | [4天直播-文本分析与爬虫](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 2020.3.28-29日; 4.4-5日 |
| **空间计量** | 范巧    | 已上线，[T1. 空间计量全局模型](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e) |
|     |     | 已上线，[T2. 地理加权回归模型(GWR)](https://lianxh.duanshu.com/#/brief/course/a62ca9dcb276496097922a61fcf1a701) |
|     | [T2-T5全程](https://lianxh.duanshu.com/#/brief/course/958fd224da8548e1ba7ff0740b536143)      | 3月21日，[T3. 内生时空权重矩阵](https://lianxh.duanshu.com/#/brief/course/94a5361647384a18852d28d1b9246362) |
| 动态模型    |     | 4月05日，[T4. 空间面板模型及动态设定](https://lianxh.duanshu.com/#/brief/course/f4e4b6b1e77c4ff88cecb685bbde07c3) |
| 空间DID    |     | 4月11日，[T5. 双重差分空间计量模型(SDID)](https://lianxh.duanshu.com/#/brief/course/ff7dc9e0b82b40dab2047af0d01e96d0) |
| 论文重现 | 连玉君    | [我的甲壳虫-经典论文精讲](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc)   |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)   |
|     |     | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，1小时40分]  |
| 大数据 | 李兵 | [经济学中的大数据应用](https://lianxh.duanshu.com/#/brief/course/da1a75bc3acc4e238f489af3367efa26) |
| R 语言 | 游万海 | [R 语言初识](https://lianxh.duanshu.com/#/brief/course/a719037536de4812a630a599f8cd7b43), 9.9元

> Note: 课程详情可以前往 [连享会-课程主页](https://www.lianxh.cn/news/46917f1076104.html) 查看。

 <img style="width: 180px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/行走的人小.jpg">





&emsp;

---
>#### 关于我们

> ##### 导航： 📍 [连享会主页](https://www.lianxh.cn)  | 📍 [知乎专栏](https://www.zhihu.com/people/arlionn/) | 📍 [直播课](http://lianxh.duanshu.com) 


